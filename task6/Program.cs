﻿using System.Xml;

namespace task6
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string fileName = "TelephoneBook.xml";

            using (var writer = new XmlTextWriter(fileName, null))
            {
                writer.Formatting = Formatting.Indented;
                writer.IndentChar = '\t';
                writer.Indentation = 1;
                writer.QuoteChar = '\'';

                writer.WriteStartDocument();
                writer.WriteStartElement("MyContacts");

                writer.WriteStartElement("Contact");
                writer.WriteStartAttribute("TelephoneNumber");
                writer.WriteString("1231231231");
                writer.WriteEndAttribute();
                writer.WriteString("John Doe");
                writer.WriteEndElement();

                writer.WriteStartElement("Contact");
                writer.WriteStartAttribute("TelephoneNumber");
                writer.WriteString("0903201");
                writer.WriteEndAttribute();
                writer.WriteString("Adam Ross");
                writer.WriteEndElement();

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }
    }
}