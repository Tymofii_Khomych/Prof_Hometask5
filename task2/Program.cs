﻿using System.Xml;
namespace task2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var doc = new XmlDocument();
            doc.Load("books.xml");

            Console.WriteLine(doc.InnerXml);
        }
    }
}