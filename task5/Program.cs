﻿namespace task5
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // XML in .NET: .NET Framework XML Classes
            // https://learn.microsoft.com/ru-ru/previous-versions/bb985993(v=msdn.10)

            /*
             *    XML is truly a core technology substrate in .NET. All other parts of the .NET Framework (ASP .NET, Web Services, and so on) 
             *    use XML as their native data representation format. The .NET Framework XML classes are also tightly coupled with 
             *    Managed Data Access (ADO .NET). Traditionally, there have always been different programming models for working with relational 
             *    versus hierarchical data. .NET breaks that tradition by offering a more deeply integrated programming model for all types of data.
             *    
             *    Abstract Base Classes
             *      At the core of the .NET Framework XML classes are two abstract classes: XmlReader and XmlWriter. XmlReader provides a fast, 
             *      forward-only, read-only cursor for processing an XML document stream. XmlWriter provides an interface for producing XML 
             *      document streams that conform to the W3C's XML 1.0 + Namespaces Recommendations. Applications that want to process XML 
             *      documents consume XmlReader, while applications that want to produce XML documents consume XmlWriter. Both classes imply 
             *      a streaming model that doesn't require an expensive in-memory cache. This makes them both attractive alternatives to the 
             *      classic DOM approach.
             *      
             *      Both XmlReader and XmlWriter are abstract base classes, which define the functionality that all derived classes must support. 
             *      There are three concrete implementations of XmlReader—XmlTextReader, XmlNodeReader, and XslReader—as well as two concrete 
             *      implementations of XmlWriter: XmlTextWriter and XmlNodeWriter (XmlNodeWriter is not in Beta 1, but should be coming in a future release.)
             *      XmlTextReader and XmlTextWriter support reading from and writing to a text-based stream, while XmlNodeReader and XmlNodeWriter 
             *      are designed for working with in-memory DOM trees (see Figure 2). One of the biggest advantages of this new design is that custom 
             *      readers and writers can be developed to extend the built-in functionality.
             *      
             *      XmlReader
             *      XmlReader provides a fast, forward-only cursor for reading XML documents. It hides the complexities of working with the 
             *      underlying data by serving up the document's Infoset through well-defined methods. At first glance, XmlReader feels a lot like 
             *      SAX (especially ContentHandler), which is also based on streaming documents, but the two programming models are fundamentally different.
             *      
             *      XmlWriter
             *      Like XmlReader, XmlWriter is an abstract class that defines the base functionality required to produce document streams conforming 
             *      to the W3C's XML 1.0 + Namespaces Recommendations. As illustrated by the earlier example, XmlWriter completely shields applications 
             *      from the complexities of producing XML document streams by allowing them to work with an Infoset-based API. Producing a document 
             *      with XmlWriter is very similar to producing documents via SAX (see the SAX XMLWriter helper class). Currently there are two 
             *      implementations of XmlWriter: XmlTextWriter and XmlNodeWriter (which is coming in a future release). These implementations are 
             *      just like the reader versions, only they work in the opposite directions.
             */
        }
    }
}