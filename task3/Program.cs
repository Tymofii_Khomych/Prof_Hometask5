﻿using System.Xml;
namespace task3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string fileName = "TelephoneBook.xml";

            using (var reader = XmlReader.Create(fileName))
            {
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "Contact")
                    {
                        string telephoneNumber = reader.GetAttribute("TelephoneNumber");
                        Console.WriteLine("TelephoneNumber: " + telephoneNumber);
                    }
                }
            }
        }
    }
}